

// https://www.javascripttutorial.net/javascript-dom/javascript-form-validation/
console.log("in fn"); 
const contactnameEl = document.querySelector('#contactname');
const emailEl = document.querySelector('#email');
const messageEl = document.querySelector('#message');
const phoneEl = document.querySelector('#phone');
const form = document.querySelector('#contact_form');


const checkName = () => {
    console.log("check name"); 
    let valid = false;

    const min = 5,
        max = 55;

    const contactname = contactnameEl.value.trim();

    if (!isRequired(contactname)) {
        console.log("contact name required - not blank"); 
        showError(contactnameEl, 'Your name cannot be blank.');
    } else if (!isBetween(contactname.length, min, max)) {
        console.log("contact name len" + contactname.length); 
        showError(contactnameEl, `Your name must be between ${min} and ${max} characters.`)
    } else {
        console.log("contact name GOOD" ); 
        showSuccess(contactnameEl);
        valid = true;
    }
    return valid;
};

const checkMsg = () => {
    console.log("check MSG"); 
    let valid = false;

    const message = messageEl.value.trim();

    if (!isRequired(message)) {
        console.log("message  required - not blank"); 
        showError(messageEl, 'Please write message before sending.');
        valid = false;
    } else {
        console.log("msg GOOD" ); 
        showSuccess(messageEl);
        valid = true;
    }
    return valid;
};


const checkEmail = () => {
    let valid = false;
    const email = emailEl.value.trim();
    if (!isRequired(email)) {
        console.log("email  required - not blank"); 
        showError(emailEl, 'Email cannot be blank.');
    } else if (!isEmailValid(email)) {
        console.log("email NOT VALId"); 
        showError(emailEl, 'Email is not valid.')
    } else {
        console.log("email GOOD"); 
        showSuccess(emailEl);
        valid = true;
    }
    return valid;
};

const checkPhone = () => {
    let valid = false;
    const phone = phoneEl.value.trim();
    if (!isRequired(phone)) {
        console.log("phone  required - not blank"); 
        showError(phoneEl, 'Please enter your phone number.');
    } else if (!isPhoneValid(phone)) {
        console.log("phone NOT VALId"); 
        showError(phoneEl, 'This phone number is not valid.')
    } else {
        console.log("phone GOOD"); 
        showSuccess(phoneEl);
        valid = true;
    }
    return valid;
};
const isPhoneValid = (phone) => {
    const re = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/;
    return re.test(phone);
};


const isEmailValid = (email) => {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
};


const isRequired = value => value === '' ? false : true;
const isBetween = (length, min, max) => length < min || length > max ? false : true;




const showError = (input, message) => {
    console.log("sherror " + message);
    document.getElementById('contact_submit').disabled  = true;
    // get the form-field element
    const formField = input.parentElement;
    // add the error class
    formField.classList.remove('success');
    formField.classList.add('error');

    // show the error message
    const error = formField.querySelector('small');
    error.textContent = message;
};

const showSuccess = (input) => {
    console.log("shSucesss " );
    document.getElementById('contact_submit').disabled = false;
    // get the form-field element
    const formField = input.parentElement;

    // remove the error class
    formField.classList.remove('error');
    formField.classList.add('success');

    // hide the error message
    const error = formField.querySelector('small');
    error.textContent = '';
}

form.addEventListener('submit', function (e) {
    // prevent the form from submitting
    e.preventDefault();
    document.getElementById('contact_submit').disabled = true;


    // validate forms
    let isNameValid = checkName();
    let  isMsgValid = checkMsg();
    let isEmailValidx = checkEmail();
    let isPhoneValidx = checkPhone();
    console.log ("isFormValid->" + isFormValid + "isNameValid- " + isNameValid +  " && isMsgValid-" + isMsgValid + " && isEmailValidx " + isEmailValidx + " && isPhoneValidx " + isPhoneValidx);    

    let isFormValid = isNameValid && isMsgValid && isEmailValidx && isPhoneValidx;

    // submit to the server if the form is valid
    if (isFormValid) {
        console.log("form VALID"); 
    }
    else {
        console.log("form INVALID"); 
    }
    
});


const debounce = (fn, delay = 500) => {
    let timeoutId;
    return (...args) => {
        // cancel the previous timer
        if (timeoutId) {
            clearTimeout(timeoutId);
        }
        // setup a new timer
        timeoutId = setTimeout(() => {
            fn.apply(null, args)
        }, delay);
    };
};
// 
form.addEventListener('input', debounce(function (e) {
    switch (e.target.id) {
        case 'contactname':
            checkName();
            break;
        case 'phone':
            checkPhone();
            break;
        case 'message':
            checkMsg();
            break;
        case 'email':
            checkEmail();
            break;
   
    }
}));